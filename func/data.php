<?php

include('./config.php');
include('grab.php');

//permalink
$cekDomain = str_replace('http://','',$domain);
$realDomain = 'http://'.$cekDomain;
$singlePerm = $realDomain.'/product/';
$catPerm = $realDomain.'/category-product/';
$tagPerm = $realDomain.'/tag-product/';

$affLink = 'http://shop.aff-id.com/aff_c?offer_id='.$categoryAff.'&aff_id='.$idAff.'&source=aliexpress&aff_sub='.$subIDAff;

if($numCategory == TRUE){ $numCat = $numCategory; }else{ $numCat = 15; }
$dirTemplate = $realDomain.'/template/'.$templateWeb;
$webTemplate = 'template/'.$templateWeb.'/index.php';
$arrayDel = array("<",">","'","$","?","+",'"',",","\"",";","!","@","#","%","^","&","*","(",")",":","|","{","}","[","]",".",",","~","`","-","/","\/","=","\�","\�");
$HTMLascii = array('&#39;','&quot;','&#44;','&lt;','&gt;');
$HTMLreal = array("'",'"',",",'<','>');

if($cDatabase == 'JSON'){
	$linkJSON = 'http://apiali.com';
	$cekKey = 'key='.$APIkey.'&';
}else{
	$linkJSON = $realDomain.'/func';
	$cekKey = '';
}

	$datahal = $numCat;
	if(isset($_GET['p'])){
		$noPage = $_GET['p'];
	}else{
		$noPage = 1;
	}
	$offset = ($noPage - 1) * $datahal;

//Detail Item
if(isset($_GET['i'])){
	$itemView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'item='.$_GET['i']), true);
		if($itemView['id'] == TRUE){
			$itemID = $itemView['id'];
			$itemTitle = $itemView['title'];
			$itemSource = $singlePerm . $itemID.'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$itemView['title'])))) . $suffPerm;
			$itemRealPrice = $itemView['real_price'];
			$itemDiscPrice = $itemView['discount_price'];
			$itemUnit = $itemView['unit'];
			$itemDisc = $itemView['discount'];
			if($itemView['rating'] == TRUE){
				$itemRating = substr($itemView['rating'],0,1);
			}else{
				$itemRating = '';
			}
					
			$numImage = count($itemView['images']);
				if($numImage == TRUE){
					for($im=0;$im<$numImage;$im++){
						$itemPict[] = $itemView['images'][$im];
						$itemTumb[] = $itemView['images'][$im].'_200x200.jpg';
					}
				}
			
			$numSpec = count($itemView['specifics']);
				if($numSpec == TRUE) {
					foreach($itemView['specifics'] as $s=>$itemView['specifics']){
						$specName[] = $s;
						$specValue[] = $itemView['specifics'];
					}
				}
							
			$urlCatItem = $catPerm . $itemView['category']['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$itemView['category']['name'])))) . $suffPerm;
			$nameCatItem = $itemView['category']['name'];
			$itemSubCat = count($itemView['category']['sub_category']);
			if(count($itemView['category']['sub_category']) == TRUE){
				$urlSubCatItem = $catPerm . $itemView['category']['sub_category']['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$itemView['category']['sub_category']['name'])))) . $suffPerm;
				$nameSubCatItem = $itemView['category']['sub_category']['name'];
				$mPoduct = $itemView['category']['sub_category']['id'];
			}else{
				$urlSubCatItem = '';
				$nameSubCatItem = '';
				$mPoduct = '';
			}
			
		}else{
			$itemID = '';
			$itemTitle = '';
			$itemSource = '';
			$itemRealPrice = '';
			$itemDiscPrice = '';
			$itemUnit = '';
			$itemDisc = '';
			$itemRating = '';
			$numImage = '';
			$numSpec = '';
				$urlSubCatItem = '';
				$nameSubCatItem = '';
				$mPoduct = '';
		}
	//Tag Random
	$cTag = preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$itemTitle))));
		$tagView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'list=keyword&random=y&keyword='.$cTag.'&aff='.$categoryAff.'&viewpage=25'), true);
			$numCatTag = count($tagView['results']);
			if($numCatTag == TRUE){
				if($numCatTag >= 25){
					$limTag = 25;
				}else{
					$limTag = $numCatTag;
				}
					for($tr=0;$tr<$numCatTag;$tr++){
						$nameTag[] = $tagView['results'][$tr]['name'];
						$urlTag[] = $tagPerm . $tagView['results'][$tr]['source'] . $suffPerm;
					}
					
			}
			
			$vr = 20;
			$vp = 3 + $vr;
			//MORE PRODUCTS
			$mItemView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'product=keyword&keyword='.$cTag.'&grab=n&viewpage='.$vp.'&random=y'), true);
				$cekmItem = (count($mItemView['results'])) - $vr;
				for($mi=0;$mi<$cekmItem;$mi++){
					$mItemID[] = $mItemView['results'][$mi]['id'];
					$mItemTitle[] = $mItemView['results'][$mi]['title'];
					$mItemSource[] = $singlePerm . $mItemView['results'][$mi]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$mItemView['results'][$mi]['title'])))) . $suffPerm;
					$mItemRealPrice[] = $mItemView['results'][$mi]['real_price'];
					$mItemDiscPrice[] = $mItemView['results'][$mi]['discount_price'];
					$mItemPict[] = $mItemView['results'][$mi]['images'][0];
					$mItemTumb[] = $mItemView['results'][$mi]['images'][0].'_200x200.jpg';
				}

	//Recomended Random
	for($r=3;$r<count($mItemView['results']);$r++){
		$reTitle[] = $mItemView['results'][$r]['title'];
		$reSingle[] = $singlePerm . $mItemView['results'][$r]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$mItemView['results'][$r]['title'])))) . $suffPerm;
		$reRealPrice[] = $mItemView['results'][$r]['real_price'];
		$reDiscPrice[] = $mItemView['results'][$r]['discount_price'];
		$reTumb[] = $mItemView['results'][$r]['images'][0].'_200x200.jpg';
		$rePict[] = $mItemView['results'][$r]['images'][0];
	}
	
	$metaPat = array();
		$metaPat[0] = '/\[TITLE\]/';
		$metaPat[1] = '/\[PRICE\]/';
	$metaRep = array();
		$metaRep[0] = str_replace($arrayDel,'',$itemTitle);
		if($itemDiscPrice == TRUE){
			$pricePat = $itemDiscPrice;
		}else{
			$pricePat = $itemRealPrice;
		}
		if($pricePat  == TRUE){
			$metaRep[1] = 'US &#36;'.$pricePat;
		}else{
			$metaRep[1] = 'you can contact supplier';
		}
		
	$numVHome = count($itemTitle);
	$singleDesc = preg_replace($metaPat, $metaRep, $caption);
	$metaTitle = preg_replace($metaPat, $metaRep, $metaTitleSingle).' | '.$webtitle;
	$metaDesc = preg_replace($metaPat, $metaRep, $metaDescSingle);
	$metaKeyw = preg_replace($metaPat, $metaRep, $metaKeywordsSingle);
	$canonical = $itemSource;
}

//View Tag
if(isset($_GET['t'])){
	$vr = 20;
	$vp = $datahal + $vr;
	$cekTagQuery = explode('-',$_GET['t']);
		$numTag = count($cekTagQuery);
		$titleCat = ucwords(str_replace('-',' ',$_GET['t']));
		$urlPage = $tagPerm . $_GET['t'].'/';

		$tagQuery = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'product=keyword&keyword='.$_GET['t'].'&page='.$noPage.'&viewpage='.$vp.'&random=y'), true);
			$countTag = count($tagQuery['results']) - $vr;
			$xCount = ceil($countTag/3);
			$yCount = $countTag%3;
			$countAllTag = $tagQuery['total_item'];
			$allCount = ceil($countAllTag/$datahal);
			for($vt=0;$vt<$countTag;$vt++){
				$tagTitle[] = $tagQuery['results'][$vt]['title'];
				$tagSingle[] = $singlePerm . $tagQuery['results'][$vt]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][$vt]['title'])))) . $suffPerm;
				$tagRealPrice[] = $tagQuery['results'][$vt]['real_price'];
				$tagDiscPrice[] = $tagQuery['results'][$vt]['discount_price'];
				$tagTumb[] = $tagQuery['results'][$vt]['images'][0].'_200x200.jpg';
			}
			
	//Recomended Random
	for($r=$datahal;$r<count($tagQuery['results']);$r++){
		$reTitle[] = $tagQuery['results'][$r]['title'];
		$reSingle[] = $singlePerm . $tagQuery['results'][$r]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][$r]['title'])))) . $suffPerm;
		$reRealPrice[] = $tagQuery['results'][$r]['real_price'];
		$reDiscPrice[] = $tagQuery['results'][$r]['discount_price'];
		$reTumb[] = $tagQuery['results'][$r]['images'][0].'_200x200.jpg';
		$rePict[] = $tagQuery['results'][$r]['images'][0];
	}

	//Tag Random
	$cekTag = array();
	for($ct=0;$ct<3;$ct++){
		$cekTag[] = preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][$ct]['title']))));
	}
		$cTag = implode("-",$cekTag);
		$tagView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'list=keyword&random=y&keyword='.$cTag.'&aff='.$categoryAff.'&viewpage=25'), true);
			$numCatTag = count($tagView['results']);
			if($numCatTag == TRUE){
				if($numCatTag >= 25){
					$limTag = 25;
				}else{
					$limTag = $numCatTag;
				}
					for($tr=0;$tr<$numCatTag;$tr++){
						$nameTag[] = $tagView['results'][$tr]['name'];
						$urlTag[] = $tagPerm . $tagView['results'][$tr]['source'] . $suffPerm;
					}
					
			}
				
	$metaPat = array();
		$metaPat[0] = '/\[TITLE\]/';
	$metaRep = array();
		$metaRep[0] = str_replace($arrayDel,'',$titleCat);
		
	$numVHome = count($tagQuery['results']);
	$metaTitle = preg_replace($metaPat, $metaRep, $metaTitleSingle).' | '.$webtitle;
	$metaDesc = preg_replace($metaPat, $metaRep, $metaDescSingle);
	$metaKeyw = preg_replace($metaPat, $metaRep, $metaKeywordsSingle);
	$canonical = $tagPerm . $_GET['t'] . $suffPerm;
}

//View Category
if(isset($_GET['c'])){
	$vr = 20;
	$vp = $datahal + $vr;
	$tagQuery = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'product=category&catid='.$_GET['c'].'&page='.$noPage.'&viewpage='.$vp.'&random=y'), true);;
		$countTag = count($tagQuery['results']) - $vr;
		$xCount = ceil($countTag/3);
		$yCount = $countTag%3;
		$countAllTag = $tagQuery['total_item'];
		$allCount = ceil($countAllTag/$datahal);
		for($t=0;$t<$countTag;$t++){
			$tagTitle[] = $tagQuery['results'][$t]['title'];
			$tagSingle[] = $singlePerm . $tagQuery['results'][$t]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][$t]['title'])))) . $suffPerm;
			$tagRealPrice[] = $tagQuery['results'][$t]['real_price'];
			$tagDiscPrice[] = $tagQuery['results'][$t]['discount_price'];
			$tagTumb[] = $tagQuery['results'][$t]['images'][0].'_200x200.jpg';
		}
	$numTag = $tagQuery['total_item'];;
	$titleCat = $tagQuery['results'][0]['category']['name'];
	$urlPage = $catPerm . $tagQuery['results'][0]['category']['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][0]['category']['name'])))).'/';
	$urlCat = $catPerm . $tagQuery['results'][0]['category']['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][0]['category']['name'])))) . $suffPerm;;
		
	//Recomended Random
	for($r=$datahal;$r<count($tagQuery['results']);$r++){
		$reTitle[] = $tagQuery['results'][$r]['title'];
		$reSingle[] = $singlePerm . $tagQuery['results'][$r]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][$r]['title'])))) . $suffPerm;
		$reRealPrice[] = $tagQuery['results'][$r]['real_price'];
		$reDiscPrice[] = $tagQuery['results'][$r]['discount_price'];
		$reTumb[] = $tagQuery['results'][$r]['images'][0].'_200x200.jpg';
		$rePict[] = $tagQuery['results'][$r]['images'][0];
	}
			
	//Tag Random
	$cekTag = array();
	for($ct=0;$ct<3;$ct++){
		$cekTag[] = preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$tagQuery['results'][$ct]['title']))));
	}
		$cTag = implode("-",$cekTag);
		$tagView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'list=keyword&random=y&keyword='.$cTag.'&aff='.$categoryAff.'&viewpage=25'), true);
			$numCatTag = count($tagView['results']);
			if($numCatTag == TRUE){
				if($numCatTag >= 25){
					$limTag = 25;
				}else{
					$limTag = $numCatTag;
				}
					for($tr=0;$tr<$numCatTag;$tr++){
						$nameTag[] = $tagView['results'][$tr]['name'];
						$urlTag[] = $tagPerm . $tagView['results'][$tr]['source'] . $suffPerm;
					}
					
			}
			
	$metaPat = array();
		$metaPat[0] = '/\[TITLE\]/';
	$metaRep = array();
		$metaRep[0] = str_replace($arrayDel,'',$titleCat);
		
	$numVHome = count($tagQuery['results']);
	$metaTitle = preg_replace($metaPat, $metaRep, $metaTitleSingle).' | '.$webtitle;
	$metaDesc = preg_replace($metaPat, $metaRep, $metaDescSingle);
	$metaKeyw = preg_replace($metaPat, $metaRep, $metaKeywordsSingle);
	$canonical = $urlCat;
}

//Search
if(isset($_POST['search'])){
	$tag = str_replace($arrayDel,'',$_POST['search']);
	$tagSource = preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$_POST['search']))));
	if($_POST['search'] == TRUE){
		Header('Location: '.$tagPerm . $tagSource . $suffPerm);
	}else{
		Header('Location: '.$realDomain);
	}
}

//List Category
$catView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'list=category&aff='.$categoryAff), true);
	$numCategory = $catView['total_item'];
	for($c=0;$c<$numCategory;$c++){
		$catName[] = $catView['results'][$c]['name'];
		$catURL[] = $catPerm . $catView['results'][$c]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$catView['results'][$c]['name'])))) . $suffPerm;
		$countSub = count($catView['results'][$c]['sub_category']);
			for($cs=0;$cs<$countSub;$cs++){
				$subName[] = $catView['results'][$c]['sub_category'][$cs]['name'];
			}
	}

//META
if(!isset($_GET['i']) && !isset($_GET['t']) && !isset($_GET['c'])){
	$metaTitle = $webtitle;
	$metaDesc = $webdescription;
	$canonical = $realDomain;
	
	$vr = 20;
	$vp = $numCat + $vr;
	
	$homeCek = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'product=category&aff='.$categoryAff.'&viewpage='.$vp.'&random=y'), true);
		$numVHome = $homeCek['total_item'];
		
	if($numVHome >= 1){
			if($numVHome >= $numCat){
				$numHome = $numCat;
			}else{
				$numHome = $numVHome;
			}
			$countHome = (count($homeCek['results'])) - $vr;
			$chCount = ceil($countHome/3);
			$hCount = $countHome%3;
			for($h=0;$h<$countHome;$h++){
				$homeTitle[] = $homeCek['results'][$h]['title'];
				$homeSingle[] = $singlePerm . $homeCek['results'][$h]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$homeCek['results'][$h]['title'])))) . $suffPerm;
				$homeRealPrice[] = $homeCek['results'][$h]['real_price'];
				$homeDiscPrice[] = $homeCek['results'][$h]['discount_price'];
				$homeTumb[] = $homeCek['results'][$h]['images'][0].'_200x200.jpg';
			}
			
			//Recomended Random
			for($r=$numCat;$r<count($homeCek['results']);$r++){
				$reTitle[] = $homeCek['results'][$r]['title'];
				$reSingle[] = $singlePerm . $homeCek['results'][$r]['id'].'/'.preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$homeCek['results'][$r]['title'])))) . $suffPerm;
				$reRealPrice[] = $homeCek['results'][$r]['real_price'];
				$reDiscPrice[] = $homeCek['results'][$r]['discount_price'];
				$reTumb[] = $homeCek['results'][$r]['images'][0].'_200x200.jpg';
				$rePict[] = $homeCek['results'][$r]['images'][0];
			}
			
			//Tag Random
			$cekTag = array();
			for($ct=0;$ct<3;$ct++){
				$cekTag[] = preg_replace('/\s+/','-',str_replace($arrayDel,'',strtolower(str_replace($HTMLascii,'',$homeCek['results'][$ct]['title']))));
			}
				$cTag = implode("-",$cekTag);
			$tagView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'list=keyword&random=y&keyword='.$cTag.'&aff='.$categoryAff.'&viewpage=25'), true);
				$numCatTag = count($tagView['results']);
				if($numCatTag == TRUE){
					if($numCatTag >= 25){
						$limTag = 25;
					}else{
						$limTag = $numCatTag;
					}
						for($tr=0;$tr<$numCatTag;$tr++){
							$nameTag[] = $tagView['results'][$tr]['name'];
							$urlTag[] = $tagPerm . $tagView['results'][$tr]['source'] . $suffPerm;
						}
					
					if(!isset($_GET['i']) && !isset($_GET['t'])){
						if($numVHome == TRUE){
							$mkey = array();
							for($x=0;$x<6;$x++){ $mkey[] = $nameTag[$x]; }
							$metaKeyw = implode(', ',$mkey);
						}else{
							$metaKeyw = '';
						}
					}
				}else{
					$metaKeyw = '';
				}
	}
}

//Detail Item
if(isset($_GET['lp'])){
	$CartView = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'item='.$_GET['lp']), true);
		$cartAliURL = $CartView['url_aliexpress'];
		$cartAliTumb = $CartView['images'][0];
}

?>