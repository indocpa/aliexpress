                <div id="main">
					<h3>Setting</h3>
                    	<form method="POST" class="jNice">
						<fieldset>
							<p>tag. [TITLE] adalah tempat di mana nama produk/keyword akan muncul dan tag. [PRICE] adalah tempat di mana harga akan muncul<br/>
							<i>tag. [PRICE] hanya bisa dipakai di CAPTION SINGLE PAGE</i></p>
							<p>
								<label>Template:</label>
								<select name="template">
									<?php 
									for($t=2;$t<count($dirTemplate);$t++){ 
										if($dirTemplate[$t] == $templateWeb){
											$select = 'selected';
										}else{
											$select = '';											
										}
									?>
									<option value="<?php echo $dirTemplate[$t]; ?>" <?php echo $select; ?>><?php echo $dirTemplate[$t]; ?></option>
									<?php } ?>
								</select>
							</p>
							<p>
								<label>View Item per Page:</label>
								<input type="text" name="num_post" value="<?php echo $numCategory; ?>" class="text-small" />
							</p>
							<p>
								<label>Suffix:</label>
								<input type="text" name="suffix" value="<?php echo $suffPerm; ?>" class="text-small" />
							</p>
							<p>
								<label>CPA Button:</label>
								<input type="text" name="cpa_button" value="<?php echo $cpaButton; ?>" class="text-medium" />
							</p>
							<p>
								<label>Meta Google Webmaster:</label>
								<input type="text" name="meta_google" value="<?php echo $metaGoogle; ?>" class="text-long" />
							</p>
							<p>
								<label>Title Website:</label>
								<input type="text" name="web_title" value="<?php echo $webtitle; ?>" class="text-long" />
							</p>
							<p>
								<label>Description Website:</label>
								<input type="text" name="web_desc" value="<?php echo $webdescription; ?>" class="text-long" />
							</p>
							<p>
								<label>Caption Single Page:</label>
								<input type="text" name="caption" value="<?php echo $caption; ?>" class="text-long" />
							</p>
							<p>
								<label>Title Single Page:</label>
								<input type="text" name="single_title" value="<?php echo $metaTitleSingle; ?>" class="text-long" />
							</p>
							<p>
								<label>Description Single Page:</label>
								<input type="text" name="single_desc" value="<?php echo $metaDescSingle; ?>" class="text-long" />
							</p>
							<p>
								<label>Keyword Single Website:</label>
								<input type="text" name="key_single" value="<?php echo $metaKeywordsSingle; ?>" class="text-long" />
							</p>
							<p>
								<label>Title Category Website:</label>
								<input type="text" name="cat_title" value="<?php echo $metaTitleTag; ?>" class="text-long" />
							</p>
							<p>
								<label>Description Category Website:</label>
								<input type="text" name="cat_desc" value="<?php echo $metaDescTag; ?>" class="text-long" />
							</p>
							<p>
								<label>Keyword Category Website:</label>
								<input type="text" name="key_cat" value="<?php echo $metaKeywordsTag; ?>" class="text-long" />
							</p>
							<p>
								<label>Histats Source Code:</label>
								<textarea name="histats_code" class="text-long" /><?php echo $histats; ?></textarea>
							</p>
							<input type="submit" value="SUBMIT" />
                    	</fieldset>
						</form>
                </div>
                <!-- // #main -->