<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <title><?php echo $metaTitle; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo $metaDesc; ?>">
<meta name="keywords" content="<?php echo $metaKeyw; ?>">
<meta name="author" content="<?php echo $cekDomain; ?>">
<?php echo $metaGoogle; ?>
<link rel="canonical" href="<?php echo $canonical; ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,400italic,600,600italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Crete+Round' rel='stylesheet' type='text/css'>
<link href="<?php echo $dirTemplate; ?>/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo $dirTemplate; ?>/css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php echo $dirTemplate; ?>/css/style.css" rel="stylesheet">
<link href="<?php echo $dirTemplate; ?>/css/flexslider.css" type="text/css" media="screen" rel="stylesheet"  />
<link href="<?php echo $dirTemplate; ?>/css/jquery.fancybox.css" rel="stylesheet">
<link href="<?php echo $dirTemplate; ?>/css/cloud-zoom.css" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<!-- Header Start -->
<header>
  <div class="headerstrip">
    <div class="container">
      <div class="row">
        <div class="span12">
          <a href="<?php echo $realDomain; ?>" class="logo pull-left"><img src="<?php echo $dirTemplate; ?>/img/logo.png" alt="<?php echo $webtitle; ?>" title="<?php echo $webtitle; ?>"></a>
          <!-- Top Nav Start -->
          <div class="pull-left">
            <div class="navbar" id="topnav">
              <div class="navbar-inner">
                <ul class="nav" >
                  <li><a class="myaccount" onclick="affFunction()" href="#my_account">My Account</a>
                  </li>
                  <li><a class="shoppingcart" onclick="affFunction()" href="#shopping_cart">Shopping Cart</a>
                  </li>
                  <li><a class="checkout" onclick="affFunction()" href="#checkOut">CheckOut</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- Top Nav End -->
          <div class="pull-right">
            <form class="form-search top-search" method="post">
              <input type="text" class="input-medium search-query" name="search" placeholder="Search Here">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="headerdetails">
      <div class="pull-left"></div>
      <div class="pull-right">
        <ul class="nav topcart pull-left">
          <li class="dropdown hover carticon ">
            <a href="#shopping_cart" class="dropdown-toggle" onclick="affFunction()" > Shopping Cart <span class="label label-orange font14">0 item(s)</span> - $0.00 <b class="caret"></b></a>
            <ul class="dropdown-menu topcartopen ">
              <li>
                <table>
                  <tbody>
                    <tr>
                      <td class="textright"><b>Sub-Total:</b></td>
                      <td class="textright">$0.00</td>
                    </tr>
                    <tr>
                      <td class="textright"><b>Eco Tax (0.00):</b></td>
                      <td class="textright">$0.00</td>
                    </tr>
                    <tr>
                      <td class="textright"><b>VAT (0%):</b></td>
                      <td class="textright">$0.00</td>
                    </tr>
                    <tr>
                      <td class="textright"><b>Total:</b></td>
                      <td class="textright">$0.00</td>
                    </tr>
                  </tbody>
                </table>
                <div class="well pull-right buttonwrap">
                  <a class="btn btn-orange" onclick="affFunction()" href="#view_cart">View Cart</a>
                  <a class="btn btn-orange" onclick="affFunction()" href="#checkOut">Checkout</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header>
<!-- Header End -->
<?php

if(isset($_GET['i'])){
	include_once('item.php');
}elseif(isset($_GET['t']) || isset($_GET['c'])){
	include_once('category.php');
}else{
	include_once('home.php');
}

?>

<!-- Footer -->
<footer id="footer">
  <section class="footerlinks">
    <div class="container">
      <div class="info">
        <ul>
          <li><a href="#privacy_policy" onclick="affFunction()">Privacy Policy</a>
          </li>
          <li><a href="#terms_conditions" onclick="affFunction()">Terms &amp; Conditions</a>
          </li>
          <li><a href="#affiliates" onclick="affFunction()">Affiliates</a>
          </li>
          <li><a href="#newsletter" onclick="affFunction()">Newsletter</a>
          </li>
        </ul>
      </div>
      <div id="footersocial">
        <a href="#facebook" onclick="affFunction()" title="Facebook" class="facebook">Facebook</a>
        <a href="#twitter" onclick="affFunction()" title="Twitter" class="twitter">Twitter</a>
        <a href="#linkeddin" onclick="affFunction()" title="Linkedin" class="linkedin">Linkedin</a>
        <a href="#rss" onclick="affFunction()" title="rss" class="rss">rss</a>
        <a href="#googleplus" onclick="affFunction()" title="Googleplus" class="googleplus">Googleplus</a>
        <a href="#skype" onclick="affFunction()" title="Skype" class="skype">Skype</a>
        <a href="#flickr" onclick="affFunction()" title="Flickr" class="flickr">Flickr</a>
      </div>
    </div>
  </section>
  <section class="copyrightbottom">
    <div class="container">
      <div class="row">
        <div class="span6"> <?php echo $webtitle; ?> &copy; <?php echo date('Y'); ?> </div>
        <div class="span6 textright"></div>
      </div>
    </div>
  </section>
  <a id="gotop" href="#">Back to top</a>
</footer>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo $dirTemplate; ?>/js/jquery.js"></script>
<script src="<?php echo $dirTemplate; ?>/js/bootstrap.js"></script>
<script src="<?php echo $dirTemplate; ?>/js/respond.min.js"></script>
<script src="<?php echo $dirTemplate; ?>/js/application.js"></script>
<script src="<?php echo $dirTemplate; ?>/js/bootstrap-tooltip.js"></script>
<script defer src="<?php echo $dirTemplate; ?>/js/jquery.fancybox.js"></script>
<script defer src="<?php echo $dirTemplate; ?>/js/jquery.flexslider.js"></script>
<script type="text/javascript" src="<?php echo $dirTemplate; ?>/js/jquery.tweet.js"></script>
<script  src="<?php echo $dirTemplate; ?>/js/cloud-zoom.1.0.2.js"></script>
<script  type="text/javascript" src="<?php echo $dirTemplate; ?>/js/jquery.validate.js"></script>
<script type="text/javascript"  src="<?php echo $dirTemplate; ?>/js/jquery.carouFredSel-6.1.0-packed.js"></script>
<script type="text/javascript"  src="<?php echo $dirTemplate; ?>/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript"  src="<?php echo $dirTemplate; ?>/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript"  src="<?php echo $dirTemplate; ?>/js/jquery.ba-throttle-debounce.min.js"></script>
<script defer src="<?php echo $dirTemplate; ?>/js/custom.js"></script>
<script>
function affFunction() {
	location.href = "<?php echo $affLink; ?>";
}
<?php if(isset($_GET['i'])){ ?>
function affClick() {
	location.href = '<?php echo $affLink; ?>';
	window.open('<?php echo $realDomain; ?>/cart/<?php echo $itemID . $suffPerm; ?>');
}
<?php } ?>
</script>
<?php echo $histats; ?>
</body>
</html>