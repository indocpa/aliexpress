<div id="maincontainer">
<?php if($numVHome == TRUE){ ?>
  <!-- Slider Start-->
  <section class="slider">
    <div class="container">
      <div class="html_carousel">
        <div id="mainslider3">
          <?php for($sh=8;$sh<11;$sh++){ ?>
		  <div class="item">
            <div>
              <a href="<?php echo $reSingle[$sh]; ?>"><img src="<?php echo $rePict[$sh]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$sh]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$sh]); ?>" /></a>
              <div class="text">
                <h1 class="productname"><span class="bgnone"><?php echo $reTitle[$sh]; ?></span></h1>
                <p style="margin-bottom:100px;"></p>
                <div class="productprice">
                  <?php if($homeDiscPrice[$sh] == TRUE){ ?>
				  <div class="productpageprice" >
                    <span class="spiral"></span>$<?php echo $homeDiscPrice[$sh]; ?>
				  </div>
                  <div class="productpageoldprice">Old price : $<?php echo $homeRealPrice[$sh]; ?></div>
				  <?php }else{ ?>
				  <div class="productpageprice" >
                    <span class="spiral"></span>$<?php echo $homeRealPrice[$sh]; ?>
				  </div>
				  <?php } ?>
                </div>
                <br>
                <ul class="productpagecart">
                  <li><a href="<?php echo $reSingle[$sh]; ?>" class="view">Details</a></li>
                </ul>
              </div>
            </div>
          </div>
		  <?php } ?>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
  <!-- Slider End-->
  
  <!-- Section Start-->
  <section class="container otherddetails">
    <div class="otherddetailspart">
      <div class="innerclass free">
        <h2>Free shipping</h2>
        All over in world over $200 </div>
    </div>
    <div class="otherddetailspart">
      <div class="innerclass payment">
        <h2>Easy Payment</h2>
        Payment Gatway support </div>
    </div>
    <div class="otherddetailspart">
      <div class="innerclass shipping">
        <h2>24hrs Shipping</h2>
        Free For All Customers </div>
    </div>
    <div class="otherddetailspart">
      <div class="innerclass choice">
        <h2>Over 5000 Choice</h2>
        50,000+ Products </div>
    </div>
  </section>
  <!-- Section End-->
<?php } ?>  
  <div class="container">
    <div class="row">
      <?php include_once('sidebar.php'); ?>
	  <?php if($numVHome == TRUE){ ?>
	  <!-- Featured Product-->
      <div class="span9">
        <section id="featured" class="row mt40">
          <h1 class="heading1 mt0"><span class="maintext">Featured Products</span><span class="subtext"> See Our Most featured Products</span></h1>
          <?php 
		  if($countHome >= 1){
		  for($ch=0;$ch<$chCount;$ch++){
			$st = $ch*3;
			if($ch == ($chCount+1)){
			   $ls = $st+$hCount;
			}else{
			   $ls = $st+3;
			}
		  ?>
		  <ul class="thumbnails">
            <?php for($h=$st;$h<$ls;$h++){ ?>
			<li class="span3">
              <a class="prdocutname" href="<?php echo $homeSingle[$h]; ?>"><?php echo substr($homeTitle[$h],0,25); ?>..</a>
              <div class="thumbnail">
                <a href="<?php echo $homeSingle[$h]; ?>"><img src="<?php echo $homeTumb[$h]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$homeTitle[$h]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$homeTitle[$h]); ?>"></a>
                <div class="pricetag">
                  <span class="spiral"></span><a href="<?php echo $homeSingle[$h]; ?>" class="productcart">DETAILS</a>
                  <div class="price">
                    <?php if($homeDiscPrice[$h] == TRUE){ ?>
					<div class="pricenew">$<?php echo $homeDiscPrice[$h]; ?></div>
                    <div class="priceold">$<?php echo $homeRealPrice[$h]; ?></div>
					<?php }else{ ?>
					<div class="pricenew">$<?php echo $homeRealPrice[$h]; ?></div>
					<?php } ?>
                  </div>
                </div>
              </div>
            </li>
			<?php } ?>
          </ul>
		  <?php }} ?>
        </section>
      </div>
	  <?php } ?>
    </div>
  <?php if($numVHome == TRUE){ ?>
  <section id="related" class="row">
    <div class="container">
      <h1 class="heading1"><span class="maintext">Best Seller</span></h1>
      <ul class="thumbnails">
        <?php for($tr=4;$tr<8;$tr++){ ?>
		<li class="span3">
          <a class="prdocutname" href="<?php echo $reSingle[$tr]; ?>"><?php echo substr($reTitle[$tr],0,25); ?>..</a>
          <div class="thumbnail">
            <a href="<?php echo $reSingle[$tr]; ?>"><img src="<?php echo $reTumb[$tr]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>"></a>
            <div class="pricetag">
              <span class="spiral"></span><a href="<?php echo $reSingle[$tr]; ?>" class="productcart">DETAILS</a>
              <div class="price">
                  <?php if($reDiscPrice[$tr] == TRUE){ ?>
				  <div class="pricenew">$<?php echo $reDiscPrice[$tr]; ?></div>
                  <div class="priceold">$<?php echo $reRealPrice[$tr]; ?></div>
				  <?php }else{ ?>
				  <div class="pricenew">$<?php echo $reRealPrice[$tr]; ?></div>
				  <?php } ?>
              </div>
            </div>
          </div>
        </li>
		<?php } ?>
      </ul>
    </div>
  </section>
  <?php } ?>
  
    <!-- Newsletter Signup-->
    <section id="newslettersignup" class="mt0">
      <div class="container">
        <div class="pull-left newsletter">
          <h2> Newsletters Signup</h2>
          Sign up to Our Newsletter & get attractive Offers by subscribing to our newsletters. </div>
        <div class="pull-right">
          <form class="form-horizontal" action="<?php echo $affLink; ?>">
            <div class="input-prepend">
              <input type="text" placeholder="Subscribe to Newsletter" id="inputIcon" class="input-xlarge">
              <input value="Subscribe" class="btn btn-orange" type="submit">
              Sign in
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
<!-- /maincontainer -->