	<?php
	function get_id_product($url='',$singlePerm=''){
		$exp=explode($singlePerm,$url);
		$dd=explode('/',$exp[1]);
		return $dd[0];
	}
	
	function imgreate($rtx)
		{
			for($i=0;$i<5;$i++){
				if($i<$rtx){
					$mg='fa-star';
				}else{
					$mg='fa-star-o';
				}
				$img .='<i class="fa '.$mg.'"></i>';
			}
			return $img;
	}
	?>
<!doctype html>
<html class="no-js" lang="">
    <head>
		<!-- Basic page needs
		============================================ -->	
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $metaTitle; ?></title>
        <meta name="description" content="<?php echo $metaDesc; ?>">
		<meta name="keywords" content="<?php echo $metaKeyw; ?>">
		<meta name="author" content="<?php echo $cekDomain; ?>">
		<?php echo $metaGoogle; ?>
		
		<!-- Mobile specific metas
		============================================ -->		
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Favicon
		============================================ -->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $dirTemplate; ?>/img/favicon.ico">

		<!-- FONTS
		============================================ -->
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300italic,300,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>

		<!-- CSS  -->
		
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/bootstrap.min.css">
		
		<!-- font-awesome CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/font-awesome.min.css">
		
		<!-- owl.carousel CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/owl.theme.css">
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/owl.transitions.css">
		
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/animate.css">
		
		<!-- FILTER_PRICE CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/jquery-ui.min.css">
		
		<!-- fancybox CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/fancybox/jquery.fancybox.css">

        <!-- Image Zoom CSS
		============================================ -->
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/img-zoom/jquery.simpleLens.css">
		
		<!-- Mobile menu CSS
		============================================ -->		
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/meanmenu.min.css">
		
		<!-- normalize CSS
		============================================ -->		
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/normalize.css">

        <!-- RS slider CSS
		============================================ -->
		<link rel="stylesheet" type="text/css" href="<?php echo $dirTemplate; ?>/lib/rs-plugin/css/settings.css" media="screen" />
		
		<!-- main CSS
		============================================ -->		
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/main.css">
		
		<!-- style CSS
		============================================ -->			
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/style.css">
		
		<!-- responsive CSS
		============================================ -->			
        <link rel="stylesheet" href="<?php echo $dirTemplate; ?>/css/responsive.css">
		
		<!-- modernizr js
		============================================ -->		
        <script src="<?php echo $dirTemplate; ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body class="<?php if(isset($_GET['i'])){ echo 'shop';}else{ echo 'home-9 home-10';}?>">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<?php if(isset($_GET['i'])){
			include('header_detail.php');
		 }else{ 
		   include('header.php');
		 } ?>
	<?php

	if(isset($_GET['i'])){
		include_once('item.php');
	}elseif(isset($_GET['t']) || isset($_GET['c'])){
		include_once('category.php');
	}else{
		include_once('home.php');
	}

	?>	
	
	
	<!-- FOOTER-BOTTUM-AREA START-->
	<div class="footer-bottum-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-xs-12 col-md-7 col-lg-6">
					<div class="footer-bottom-menu">
						<nav>
							<ul>
								<li><a href="#">Site Map</a></li>
								<li><a href="#">Search Terms</a></li>
								<li><a href="#">Advanced Search</a></li>
								<li><a href="#">Orders and Returns</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
						</nav>
					</div>
					<div class="copyright-info">
						Copyright &copy; 2015 <a href="#">BootExperts.</a> All Rights Reserved
					</div>
				</div>
				<div class="col-sm-12 col-xs-12 col-lg-6 col-md-5">
					<div class="footer-payment-logo">
						<img src="<?php echo $dirTemplate; ?>/img/payment/visa.png" alt="" />
						<img src="<?php echo $dirTemplate; ?>/img/payment/master.png" alt="" />
						<img src="<?php echo $dirTemplate; ?>/img/payment/paypal.png" alt="" />
						<img src="<?php echo $dirTemplate; ?>/img/payment/skrill.png" alt="" />
						<img src="<?php echo $dirTemplate; ?>/img/payment/discover.png" alt="" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- FOOTER-BOTTUM-AREA END-->

		<!-- JS -->
		
		<!-- jquery js -->
        <script src="<?php echo $dirTemplate; ?>/js/vendor/jquery-1.11.3.min.js"></script>
		
		<!-- bootstrap js -->
        <script src="<?php echo $dirTemplate; ?>/js/bootstrap.min.js"></script>
		
		<!-- owl.carousel.min js -->
        <script src="<?php echo $dirTemplate; ?>/js/owl.carousel.min.js"></script>
		
		<!-- Mobile Menu js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.meanmenu.js"></script>
		
		<!-- FILTER_PRICE js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery-ui.min.js"></script>
		
		<!-- mixitup js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.mixitup.min.js"></script>

        <!-- fancybox js -->
		<script src="<?php echo $dirTemplate; ?>/js/fancybox/jquery.fancybox.pack.js"></script>

		<!-- Img Zoom js -->
		<script src="<?php echo $dirTemplate; ?>/js/img-zoom/jquery.simpleLens.min.js"></script>
		
		<!-- jquery.countdown js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.countdown.min.js"></script>
		
		<!-- parallax js -->
        <script src="<?php echo $dirTemplate; ?>/js/parallax.js"></script>	

		<!-- jquery.collapse js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.collapse.js"></script>
		
		<!-- jquery.easing js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.easing.1.3.min.js"></script>	
		
		<!-- jquery.scrollUp js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.scrollUp.min.js"></script>	
		
		<!-- knob circle js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.knob.js"></script>	
		
		<!-- jquery.appear js -->
		<script src="<?php echo $dirTemplate; ?>/js/jquery.appear.js"></script>			

		<!-- jquery.counterup js -->
        <script src="<?php echo $dirTemplate; ?>/js/jquery.counterup.min.js"></script>
        <script src="<?php echo $dirTemplate; ?>/js/waypoints.min.js"></script>		
		
		<!-- wow js -->
        <script src="<?php echo $dirTemplate; ?>/js/wow.js"></script>		
		<script>
			new WOW().init();
		</script>

		<!-- rs-plugin js -->
		<script type="text/javascript" src="<?php echo $dirTemplate; ?>/lib/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
		<script type="text/javascript" src="<?php echo $dirTemplate; ?>/lib/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="<?php echo $dirTemplate; ?>/lib/rs-plugin/rs.home.js"></script>
		
		<!-- plugins js -->
        <script src="<?php echo $dirTemplate; ?>/js/plugins.js"></script>
		
		<!-- main js -->
        <script src="<?php echo $dirTemplate; ?>/js/main.js"></script>
		<script>
		function affFunction() {
			location.href = "<?php echo $affLink; ?>";
		}
		<?php //if(isset($_GET['i'])){ ?>
		function affClick(itemID='') {
			location.href = '<?php echo $affLink; ?>';
			if(itemID!=''){
				window.open('<?php echo $realDomain; ?>/cart/'+itemID+'<?php echo $suffPerm; ?>');
			}else{
				window.open('<?php echo $realDomain; ?>/cart/<?php echo $itemID . $suffPerm; ?>');
			}
		}
		<?php //} ?>
		</script>
		<?php echo $histats; ?>
    </body>
</html>
