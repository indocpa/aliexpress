<div class="slider-and-category-saidebar">
			<div class="container">
				<div class="row">
					
					<div class="col-lg-12 col-md-12">
						<?php if($numVHome == TRUE){ 
						
						?>
						<!-- SLIDER-AREA-START -->
						<div class="slider-wrap">
							<div class="fullwidthbanner-container" >
								<div class="fullwidthbanner-10">
									<ul>	<!-- SLIDE  -->
										<?php for($sh=8;$sh<11;$sh++){ 
											$linkslide[]=$reSingle[$sh];
											$imgslide[]=$rePict[$sh];
										?>
										<li 
											data-index="rs-28" 
											data-transition="random" 
											data-slotamount="7"  
											data-easein="default" 
											data-easeout="default"
											data-rotate="0"  
											data-saveperformance="off">
											<!-- MAIN IMAGE -->
											<img src="<?php echo $rePict[$sh]; ?>"  alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$sh]); ?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
											<!-- LAYERS -->
										</li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<!-- SLIDER-AREA-END -->
						<?php } ?>
					</div>
				</div>
			</div>
		</div> 

		<div class="saide-bar-maincontaint-area">
		<div class="container">
			<div class="row">
				
				<?php include('sidebar.php');?>
				<div class="col-lg-9 col-md-9 col-sm-9">
				<?php if($numVHome == TRUE){ ?>
				<div class="product-area">
				<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="title-tab-menu">
						<h3>FEATURED PRODUCTS</h3>
						<div class="tab-menu">
							<ul>
								<li class="active"><a href="#women" data-toggle="tab"></a></li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="tab-content">
							<div class="tab-pane fade in active" id="women">
								<div class="product-carusol-10x">
									 <?php 
									  if($countHome >= 1){
									  for($ch=0;$ch<$chCount;$ch++){
										$st = $ch*3;
										if($ch == ($chCount+1)){
										   $ls = $st+$hCount;
										}else{
										   $ls = $st+3;
										}
									?>
									<?php for($h=$st;$h<$ls;$h++){ ?>
									<div class="col-lg-3 col-md-3" style="min-height:300px;">
										<!-- SINGLE-PRODUCT START-->
										<div class="single-product">
											<div class="product-img">
												<a href="<?php echo $homeSingle[$h]; ?>">
													<img src="<?php echo $homeTumb[$h]; ?>" alt="Special" class="primary-image"/>
												</a>
												<div class="action-button">
													<div class="add-to-wishlist">
														<a class="color-tooltip" data-toggle="tooltip" href="#"  onclick="affClick(<?php echo get_id_product($homeSingle[$c],$singlePerm);?>)" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
													</div>
													<div class="compare-button">
														<a class="color-tooltip" data-toggle="tooltip" href="#"  onclick="affClick(<?php echo get_id_product($homeSingle[$c],$singlePerm);?>)" title="Compare"><i class="fa fa-files-o"></i></a>
													</div>
													<div class="quickviewbtn">
														<a class="color-tooltip" data-toggle="tooltip" href="#"  onclick="affClick(<?php echo get_id_product($homeSingle[$c],$singlePerm);?>)" title="Quick View"><i class="fa fa-search"></i></a>
													</div>
												</div>
											</div>
											<div class="product-name-ratting">
												<h2 class="product-name">
													<a href="<?php echo $homeSingle[$h]; ?>"><?php echo substr($homeTitle[$h],0,25); ?>..</a>
												</h2>
												<div class="ratings">
													<?php $rnd=rand(1,50)/10; echo imgreate($rnd);?>
												</div>
												<div class="price-box-small">
													<span class="special-price">
														$<?php echo $homeRealPrice[$h]; ?>
													</span>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT END-->
									</div>
									<?php } ?>
									<?php } } ?>
									
								</div>
							</div>
							<!-- WOMEN-TAB-END -->
							
						</div>
					</div>
				</div>
			</div>
			</div>
			<?php } ?>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="add-area-ly-10">
						<div class="single-banner-left" style="height: 300px ; overflow: hidden">
							<a href="#" onclick="affClick(<?php echo get_id_product($linkslide[0],$singlePerm);?>)">
								<img alt=""  src="<?php echo $imgslide[0]; ?>">
							</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

	<!-- PRODUCT-CAROSUL START-->
	<div class="product-area hot">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="title-tab-menu">
						<h3>HOT SALE PRODUCTS</h3>
					</div>
					<div class="row">
						<div class="product-carusol-9">
							<?php for($tr=4;$tr<8;$tr++){ ?>
							<div class="col-lg-3 col-md-3">
								<!-- SINGLE-PRODUCT START-->
								<div class="single-product">
									<span class="onsale">
										<span class="sale-bg"></span>
										<span class="sale-text">Sale</span>
									</span>
									<div class="product-img">
										<a href="<?php echo $reSingle[$tr]; ?>">
											<img src="<?php echo $reTumb[$tr]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" class="primary-image"/>
										</a>
										<div class="action-button">
											<div class="add-to-wishlist">
												<a class="color-tooltip" data-toggle="tooltip" href="#" title="Add to Wishlist"  onclick="affClick();"><i class="fa fa-heart-o"></i></a>
											</div>
											<div class="compare-button">
												<a class="color-tooltip" data-toggle="tooltip" href="#" title="Compare"><i class="fa fa-files-o" onclick="affClick();"></i></a>
											</div>
											<div class="quickviewbtn">
												<a class="color-tooltip" data-toggle="tooltip" href="#" title="Quick View"><i class="fa fa-search" onclick="affClick();"></i></a>
											</div>
										</div>
									</div>
									<div class="product-name-ratting">
										<h2 class="product-name">
											<a href="<?php echo $reSingle[$tr]; ?>"><?php echo substr($reTitle[$tr],0,25); ?>..</a>
										</h2>
										<div class="ratings">
											<?php $rnd=rand(1,50)/10; echo imgreate($rnd);?>
										</div>
										<div class="price-box-small">
											<span class="special-price">
												$<?php echo $reDiscPrice[$tr]; ?>
											</span>
											<span class="old-price">
												$<?php echo $reRealPrice[$tr]; ?>
											</span>
										</div>
									</div>
								</div>
								<!-- SINGLE-PRODUCT END-->
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- PRODUCT-CAROSUL END-->
	