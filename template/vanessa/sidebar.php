<div class="col-lg-3 col-md-3 col-sm-3">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="category-saidebar">
								<ul>
									<?php for($c=0;$c<$numCategory;$c++){ ?>
									<li>
										<a href="<?php echo $catURL[$c]; ?>">
											<i class="fa fa-plus"></i>
											<?php echo $catName[$c]; ?>
										</a>
									</li>
									<?php } ?>
								</ul>
								<!--<div class="morelesscate">
									<span class="morecate">
										<i class="fa fa-plus"></i>More Categories
									</span>
									<span class="lesscate">
										<i class="fa fa-minus"></i>Close Menu
									</span>
								</div>-->
							</div>
						</div>	
					</div>
					<?php if($numVHome == TRUE){ ?>
					<div class="bestseller-product-area">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="title-tab-menu">
								<h3>Bestseller</h3>
							</div>
							<div class="best-sellers-carosul">
								<div class="best-product-list">
									<?php for($tr=0;$tr<5;$tr++){ ?>
									<!-- SINGLE-PRODUCT START-->
									<div class="single-product">
										<div class="product-img">
											<a href="<?php echo $reSingle[$tr]; ?>">
												<img src="<?php echo $reTumb[$tr]; ?>"  alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" class="primary-image"/>
												<img class="secondary-image"  alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" src="<?php echo $reTumb[$tr]; ?>">
											</a>
										</div>
										<div class="product-name-ratting">
											<h2 class="product-name">
												<a href="<?php echo $reSingle[$tr]; ?>"><?php echo substr($reTitle[$tr],0,17); ?>...</a>
											</h2>
											<div class="ratings">
												<?php $rnd=rand(1,50)/10; echo imgreate($rnd);?>
											</div>
											<div class="price-box-small">
												<span class="special-price">
													$<?php echo $reRealPrice[$tr]; ?>
												</span>
												<!--<span class="old-price">
													$<?php //echo $reDiscPrice[$tr]; ?>
												</span>-->
											</div>
										</div>
									</div>
									<!-- SINGLE-PRODUCT END-->
									<?php } ?>
								</div>
								
							</div>
						</div>
					</div>
					</div>
					<!-- POPULAR-TAGS-AREA START-->
					<div class="popular-tags-area">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div class="popular-tags">
									<div class="title-tab-menu">
										<h3>Popular Tags</h3>
									</div>
									<ul>
										<?php for($st=0;$st<$limTag;$st++){ ?>
										<li><a href="<?php echo $urlTag[$st]; ?>"><?php echo $nameTag[$st]; ?></a></li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- POPULAR-TAGS-AREA END-->
					<?php } ?>
				</div>