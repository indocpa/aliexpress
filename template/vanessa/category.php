		<div class="saide-bar-maincontaint-area">
		<div class="container">
			<div class="row">
				
				<?php include('sidebar.php');?>
				<div class="col-lg-9 col-md-9 col-sm-9">
				<?php if($numVHome == TRUE){ ?>
				<div class="product-area">
				<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="title-tab-menu">
						<br />
						<h3><?php echo $titleCat; ?> Category</h3>
						<div class="tab-menu">
							<ul>
								<li class="active"><a href="#women" data-toggle="tab"></a></li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="tab-content">
							<div class="tab-pane fade in active" id="women">
								<div class="product-carusol-10x">
									  <?php 
									  for($gr=0;$gr<$xCount;$gr++){ 
										$st = $gr*3;
										if($gr == ($xCount-1)){
										  $ls = $st+$yCount;
										}else{
										  $ls = $st+3;
										}
									  ?>
									<?php for($c=$st;$c<$ls;$c++){ ?>
									<div class="col-lg-3 col-md-3" style="min-height:300px;">
										<!-- SINGLE-PRODUCT START-->
										<div class="single-product">
											<div class="product-img">
												<a href="<?php echo $tagSingle[$c]; ?>">
													<img src="<?php echo $tagTumb[$c]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$tagTitle[$c]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$tagTitle[$c]); ?>" class="primary-image"/>
												</a>
												<div class="action-button">
													<div class="add-to-wishlist">
														<a class="color-tooltip" data-toggle="tooltip" href="#"   onclick="affClick(<?php echo get_id_product($tagSingle[$c],$singlePerm);?>)" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
													</div>
													<div class="compare-button">
														<a class="color-tooltip" data-toggle="tooltip" href="#"  onclick="affClick(<?php echo get_id_product($tagSingle[$c],$singlePerm);?>)" title="Compare"><i class="fa fa-files-o"></i></a>
													</div>
													<div class="quickviewbtn">
														<a class="color-tooltip" data-toggle="tooltip" href="#"  onclick="affClick(<?php echo get_id_product($tagSingle[$c],$singlePerm);?>)" title="Quick View"><i class="fa fa-search"></i></a>
													</div>
												</div>
											</div>
											<div class="product-name-ratting">
												<h2 class="product-name">
													<a href="<?php echo $tagSingle[$c]; ?>"><?php echo substr($tagTitle[$c],0,25); ?>..</a>
												</h2>
												<div class="ratings">
													<?php $rnd=rand(1,50)/10; echo imgreate($rnd);?>
												</div>
												<div class="price-box-small">
													<?php if($tagDiscPrice[$c] == TRUE){ ?>
													<span class="special-price">
														$<?php echo $tagDiscPrice[$c]; ?>
													</span>
													<span class="old-price">
														$<?php echo $tagRealPrice[$c]; ?>
													</span>
													<?php }else{ ?>
													$<?php echo $tagRealPrice[$c]; ?>
													<?php } ?>
												</div>
											</div>
										</div>
										<!-- SINGLE-PRODUCT END-->
									</div>
									<?php } ?>
									<?php }  ?>
									
								</div>
							</div>
							<!-- WOMEN-TAB-END -->
							
						</div>
					</div>
				</div>
			</div>
			</div>
			<?php } ?>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="add-area-ly-10">
						<div class="single-banner-left" style="height: 300px ; overflow: hidden">
							<?php for($sh=8;$sh<11;$sh++){ 
									$linkslide[]=$reSingle[$sh];
									$imgslide[]=$rePict[$sh];
								}
							?>
							<a href="#" onclick="affClick(<?php echo get_id_product($linkslide[0],$singlePerm);?>)">
								<img alt=""  src="<?php echo $imgslide[0]; ?>">
							</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

	<!-- PRODUCT-CAROSUL START-->
	<div class="product-area hot">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="title-tab-menu">
						<h3>HOT SALE PRODUCTS</h3>
					</div>
					<div class="row">
						<div class="product-carusol-9">
							<?php for($tr=4;$tr<8;$tr++){ ?>
							<div class="col-lg-3 col-md-3">
								<!-- SINGLE-PRODUCT START-->
								<div class="single-product">
									<span class="onsale">
										<span class="sale-bg"></span>
										<span class="sale-text">Sale</span>
									</span>
									<div class="product-img">
										<a href="<?php echo $reSingle[$tr]; ?>">
											<img src="<?php echo $reTumb[$tr]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" class="primary-image"/>
										</a>
										<div class="action-button">
											<div class="add-to-wishlist">
												<a class="color-tooltip" data-toggle="tooltip" href="#" onclick="affClick(<?php echo get_id_product($reSingle[$tr],$singlePerm);?>)" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
											</div>
											<div class="compare-button">
												<a class="color-tooltip" data-toggle="tooltip" href="#" onclick="affClick(<?php echo get_id_product($reSingle[$tr],$singlePerm);?>)" title="Compare"><i class="fa fa-files-o"></i></a>
											</div>
											<div class="quickviewbtn">
												<a class="color-tooltip" data-toggle="tooltip" href="#" onclick="affClick(<?php echo get_id_product($reSingle[$tr],$singlePerm);?>)" title="Quick View"><i class="fa fa-search"></i></a>
											</div>
										</div>
									</div>
									<div class="product-name-ratting">
										<h2 class="product-name">
											<a href="<?php echo $reSingle[$tr]; ?>"><?php echo substr($reTitle[$tr],0,25); ?>..</a>
										</h2>
										<div class="ratings">
											<?php $rnd=rand(1,50)/10; echo imgreate($rnd);?>
										</div>
										<div class="price-box-small">
											<span class="special-price">
												$<?php echo $reDiscPrice[$tr]; ?>
											</span>
											<span class="old-price">
												$<?php echo $reRealPrice[$tr]; ?>
											</span>
										</div>
									</div>
								</div>
								<!-- SINGLE-PRODUCT END-->
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- PRODUCT-CAROSUL END-->
	