	<?php if($numVHome == TRUE){ ?>
	<div class="shop-content-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="breadcrumbs">
						<ul>
							<li>
								<a href="<?php echo $realDomain; ?>">Home</a> 
								<i class="fa fa-angle-right"></i>
							</li>
							<?php if($itemSubCat == '0'){ ?>
							<li>
								<a href="<?php echo $urlCatItem; ?>"><?php echo $nameCatItem; ?></a> 
								<i class="fa fa-angle-right"></i>
							</li>
							<?php }else{ ?>
							<li>
								<a href="<?php echo $urlCatItem; ?>"><?php echo $nameCatItem; ?></a> 
								<i class="fa fa-angle-right"></i>
							</li>
							<li> <a href="<?php echo $urlSubCatItem; ?>"><?php echo $nameSubCatItem; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if($numVHome == TRUE){ ?>
	<!-- PRODUCT-VIEW-AREA-START -->
	<div class="product-view-area">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-7 col-lg-7 col-md-7">
					<div class="product-datails-tab">
						<?php
						if($numImage >= 3){
							$fPic = 3;
						}else{
							$fPic = $numImage;
						}
							
						?>
						<div class="thumnail-image fix">
							<ul class="tab-menu">
								<?php for($vp=0;$vp<$fPic;$vp++){ ?>
								<li class="<?php if($vp==0){echo 'active' ;}?>"><a data-toggle="tab" href="#image<?php echo $vp;?>"><img alt="" src="<?php echo $itemPict[$vp]; ?>"></a></li>
								<?php } ?>
							</ul>
						</div>
						<div class="larg-img">
							<div class="tab-content">
								<?php for($vp=0;$vp<$fPic;$vp++){ ?>
								<div id="image<?php echo $vp;?>" class="tab-pane fade in <?php if($vp==0){echo 'active' ;}?>">
									<div class="simpleLens-big-image-container">
										<a class="simpleLens-lens-image" data-lens-image="<?php echo $itemPict[$vp]; ?>">
										<img  alt="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>" src="<?php echo $itemPict[$vp]; ?>" class="simpleLens-big-image">
										</a>
										<a class="fancybox" data-fancybox-group="group" href="<?php echo $itemPict[$vp]; ?>"><i class="fa fa-expand"></i></a>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						
					</div>				
				</div>
				<div class="col-xs-12 col-sm-5 col-lg-5 col-md-5">
					<div class="product-details-area">
						<div class="product-header">
							<div class="ratting">
								<?php $rnd=rand(1,50)/10; echo imgreate($rnd);?>
							</div>
							<!--<div class="next-prev">
								<a href="#"><i class="fa fa-angle-left"></i></a>
								<a href="#"><i class="fa fa-angle-right"></i></a>
							</div>-->
						</div>
						<div class="product-description">
							<h1><?php echo $itemTitle; ?></h1>
							<div class="short-description">
								<p class="stock-status">In stock</p>
								<p itemprop="description"><?php echo $singleDesc; ?></p>
							</div>
							<div class="price-box">
								<p>
									<?php if($itemDiscPrice == TRUE){ ?>
									<span class="special-price">
										$<?php echo $itemRealPrice; ?>
									</span>
									<span class="old-price">
										$<?php echo $itemDiscPrice; ?>
									</span>
									<?php }else{ ?>
									<span class="special-price">
										$<?php echo $itemRealPrice; ?>
									</span>
									<?php } ?>
								</p>
							</div>
							<form action="#">
								<div class="qnt-addcart">
									<input type="number" value="1">
									<button type="submit" onclick="affClick()"><i class="fa fa-shopping-cart"></i><?php echo $cpaButton; ?></button>
								</div>
							</form>
							<div class="wish-list-btn">
								<a href="#"  onclick="affClick()">
									<i class="fa fa-heart-o"></i> 
									Add to Wishlist
								</a>
							</div>
							<div class="product-meta">
								<span>
									Categories: 
									<?php for($c=0;$c<$numCategory;$c++){ ?>
										<a href="<?php echo $catURL[$c]; ?>">
											<?php echo $catName[$c]; ?>
										</a>
									<?php } ?>
								</span>
								<span>
									Tags: 
									<?php for($st=0;$st<$limTag;$st++){ ?>
										<a href="<?php echo $urlTag[$st]; ?>"><?php echo $nameTag[$st]; ?></a>,
									<?php } ?>
								</span>
							</div>
							<div class="share-buttons"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- PRODUCT-VIEW-AREA-END -->
	<?php } ?>
	
	
	
	<div class="related-product-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<h2 class="area-headding">Related Products</h2>
					<div class="row">
						<div class="product-carusol">
							<?php for($mp=0;$mp<3;$mp++){ ?>
							<div class="col-lg-3 col-md-3">
								<!-- SINGLE-PRODUCT START-->
								<div class="single-product">
									<span class="onsale">
										<span class="sale-bg"></span>
										<span class="sale-text">Sale</span>
									</span>
									<div class="product-img">
										<a href="<?php echo $mItemSource[$mp]; ?>">
											<img src="<?php echo $mItemTumb[$mp]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$mItemTitle[$mp]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$mItemTitle[$mp]); ?>" class="primary-image"/>
										</a>
										<div class="price-rate">
											<div class="ratings">
												<?php $rnd=rand(1,50)/10; echo imgreate($rnd);?>
											</div>
											<div class="price-box">
												<span class="special-price">
													$<?php echo $mItemRealPrice[$mp]; ?>
												</span>
											</div>
										</div>
										<div class="action-button">
											<div class="add-to-wishlist">
												<a class="color-tooltip" data-toggle="tooltip" href="#"  onclick="affClick()" title="Add to Wishlist"><i class="fa fa-heart-o"></i></a>
											</div>
											<div class="compare-button">
												<a class="color-tooltip" data-toggle="tooltip" href="#" onclick="affClick()" title="Compare"><i class="fa fa-files-o"></i></a>
											</div>
											<div class="quickviewbtn">
												<a class="color-tooltip" data-toggle="tooltip" href="#" onclick="affClick()" title="Quick View"><i class="fa fa-search"></i></a>
											</div>
										</div>
									</div>
									<div class="product-name-cart-button">
										<h2 class="product-name">
											<a href="<?php echo $mItemSource[$mp]; ?>"><?php echo substr($mItemTitle[$mp],0,25); ?>..</a>
										</h2>
										<a href="<?php echo $mItemSource[$mp]; ?>" class="button"  ><?php echo $cpaButton; ?></a>
									</div>
								</div>
								<!-- SINGLE-PRODUCT END-->
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>