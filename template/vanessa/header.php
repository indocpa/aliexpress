 <!--  HEADER-AREA START-->
		<div class="header_area">
			<div class="header-top-bar">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-xs-12 col-lg-5 col-md-5 col-md-5">
							<div class="header-left">
								<div class="header-email">
									<strong></strong> 
								</div>
								<div class="header-phone">
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-xs-12 col-lg-7 col-md-7">
							<div class="header-right">
								<div class="menu-top-menu">
									<ul>
										<li><a href="#" onclick="affClick()">My Account</a></li>
										<li><a href="#" onclick="affClick()">My Wishlist</a></li>
										<li><a href="#" onclick="affClick()">Shopping Cart</a></li>
										<li><a href="#" onclick="affClick()">Checkout</a></li>
									</ul>
								</div>
								<div class="lang-sel-list">
									<ul>
										<li>
											<a href="#" onclick="affClick()"><img alt="en" src="<?php echo $dirTemplate; ?>/img/en.png"></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<!-- LOGO-SEARCH-AREA START-->
				<div class="row">
					<div class="col-xs-12 col-lg-3 col-md-3">
						<div class="logo">
							<a href="<?php echo $realDomain; ?>"><img alt="" src="<?php echo $dirTemplate; ?>/img/logo-2.png"></a>
						</div>
					</div>
					<div class="col-xs-12 col-lg-9 col-md-9">
						<div class="search-cart-list">
						<div class="header-search">
							<div class="cate-toggler">Categories</div>
							<div class="product-category">
								<ul>
									<?php for($st=0;$st<$limTag;$st++){ ?>
										<li><a href="<?php echo $urlTag[$st]; ?>"><?php echo $nameTag[$st]; ?></a></li>
									<?php } ?>
								</ul>
							</div>
							<form>
								<div>
									<input type="text" placeholder="" value="Search product..." onblur="if (this.value == '') {this.value = 'Search product...';}" onfocus="if (this.value == 'Search product...') {this.value = '';}">
									<button type="submit">
										<i class="fa fa-search"></i>
									</button>
								</div>
							</form>
						</div>
						<div class="cart-total">
							<ul>
								<li><a class="cart-toggler" href="#" onclick="affClick()">
									<span class="cart-icon"></span> 
									<span class="cart-no"><i class="fa fa-shopping-cart"></i> My cart: 00 items</span></a>
									<div class="mini-cart-content">
										
										<div class="cart-inner-bottom">
											<p class="total">Subtotal: <span class="amount">$00.00</span></p>
											<div class="clear"></div>
											<p class="buttons"><a href="#" onclick="affClick()" >Checkout</a></p>
										</div>
									</div>
								</li>
							</ul>
						</div>
						</div>
					</div>
				</div>
				<!-- LOGO-SEARCH-AREA END-->
			</div>
			
			
			<!-- MAINMENU-AREA START-->
			<div class="mainmenu-area">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3">
							<div class="catemenu-toggler">
								<i class="fa fa-bars"></i>
								<span>Category</span>
							</div>
						</div>
						<div class="col-lg-9 col-md-9">
							<div class="main-menu">
								<nav>
									<ul>
										<li><a href="<?php echo $realDomain; ?>">Home<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#" onclick="affClick();">Shop<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#" onclick="affClick();">Blog<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#" onclick="affClick();">PORTFOLIO<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#" onclick="affClick();">PAGES<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- MAINMENU-AREA END-->
			<!-- MOBILE-MENU-AREA START -->
			<div class="mobile-menu-area">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="mobile-menu">
								<nav id="dropdown">
									<ul>
										<li><a href="<?php echo $realDomain; ?>">Home</a></li>
										<li><a href="#" onclick="affClick();">Shop</a></li>
										<li><a href="#" onclick="affClick();">Blog</a></li>
										<li><a href="#" onclick="affClick();">Pages</a></li>
										<li><a href="#" onclick="affClick();">Contact</a></li>
									</ul>
								</nav>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<!-- MOBILE-MENU-AREA END -->
		</div>
		<!--  HEADER-AREA START-->