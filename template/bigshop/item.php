<div class="wrapper_content">
               <div class="container_24 ">
                  <div class="clear"></div>
                  <div class="grid_24 em-breadcrumbs">
					<div class="breadcrumbs">
					  
				   </div>
				  </div>
                  <div class="grid_18 em-main-wrapper">
					   <div class="product-view">
						  <div class="product-essential">
							 <form action="<?php echo $dirTemplate; ?>/product.html" method="post" id="product_addtocart_form">
								<div class="no-display">
								   <input type="hidden" name="product" value="166">
								   <input type="hidden" name="related_product" id="related-products-field" value=""/>
								</div>
								<div class="product-img-box" style="width:480px;">
								   <p class="product-image">
									  <a class="cloud-zoom" id="image_zoom" rel="zoomWidth: 380,zoomHeight: 380,adjustX: 10, adjustY:-4" href="<?php echo $itemPict[0]; ?>">
									  <img src="<?php echo $itemPict[0]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>" alt="" title="<?php echo $itemTitle; ?>">
									  </a>
									  <a id="zoom-btn" rel="lightbox[em_lightbox]" href="<?php echo $itemPict[0]; ?>" title="<?php echo $itemTitle; ?>">Zoom</a>
								   </p>
								   <div class="slider more-views slideshow-more-views">
									  <div id="slider_moreview">
										 <ul>
											<?php foreach($itemTumb as $yy=>$dataImg){?>
											<li class="item">
											   <a class="cloud-zoom-gallery" rel="
												  useZoom:'image_zoom',               
												  smallImage:'<?php echo $itemPict[$yy];?>', 
												  adjustX:5, adjustY:-5" onclick="return false" href="<?php echo $itemPict[$yy];?>">
											   <img width="70px" height="70px" src="<?php echo $dataImg;?>" alt=""/>
											   </a>
											   <a class="no-display" href="<?php echo $itemPict[$yy];?>" rel="lightbox[em_lightbox]">lightbox moreview</a>
											</li>
											<?php }?>
										 </ul>
									  </div>
								   </div>
								   <script type="text/javascript">
									  jQuery('.cloud-zoom-gallery').click(function () {
										jQuery('#zoom-btn').attr('href', this.href);
									  });
								   </script>
								</div>
								<div class="product-shop no-related">
								   <div class="product-shop-wrapper ">
									  <a href="#" title="Previous" onclick="affClick()" class="prev">Previous</a>
									  <a href="#" title="Next" onclick="affClick()" class="next">Next</a>
									  <div class="product-name">
										 <h1><?php echo $itemTitle; ?></h1>
									  </div>
									  <p class="email-friend"><a href="#">Email to a Friend</a></p>
									  <p class="no-rating"><a href="#" class="r-lnk">Be the first to review this product</a></p>
									  
									  <p class="availability in-stock">Availability: <span>In stock</span></p>
									  <div class="price-box">
										 <span class="regular-price" id="product-price-166">
										 <span class="price">$<?php echo $itemRealPrice; ?></span>                                    </span>
									  </div>
									  <div class="short-description">
										 <h2>Quick Overview</h2>
										 <div class="std"><?php echo $singleDesc; ?></div>
									  </div>
									  <div class="add-to-box">
										 <div class="add-to-cart">
											<label for="qty">Qty:</label>
											<input type="text" name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty">
											<div class="qty-ctl">
											   <button title="Increase Qty" onclick="changeQty(1); return false;" class="increase">increase</button>
											   <button title="Decrease Qty" onclick="changeQty(0); return false;" class="decrease">decrease</button>
											</div>
											<button  onclick="affClick()" type="button" title="Add to Cart" class="button btn-cart"><span><span>Add to Cart</span></span></button>
											<script type="text/javascript">
											   function changeQty(increase) {
												var qty = parseInt($('qty').value);
												if ( !isNaN(qty) ) {
													qty = increase ? qty+1 : (qty>1 ? qty-1 : 1);
													$('qty').value = qty;
												}
											   }
											</script>
										 </div>
										 <ul class="add-to-links">
											<li><span class="separator">|</span> <a onclick="affClick()" href="#" class="link-compare" title="Add to Compare">Add to Compare</a></li>
											<li><a href="<?php echo $dirTemplate; ?>/login.html" onclick="productAddToCartForm.submitLight(this, this.href); return false;" class="link-wishlist" title="Add to Wishlist" onclick="affClick()">Add to Wishlist</a></li>
										 </ul>
									  </div>
								   </div>
								</div>
								<div class="clearer"></div>
							 </form>
							 <script type="text/javascript">
								//<![CDATA[
									var productAddToCartForm = new VarienForm('product_addtocart_form');
									productAddToCartForm.submit = function(button, url) {
										if (this.validator.validate()) {
											var form = this.form;
											var oldUrl = form.action;
								
											if (url) {
											   form.action = url;
											}
											var e = null;
											try {
												this.form.submit();
											} catch (e) {
											}
											this.form.action = oldUrl;
											if (e) {
												throw e;
											}
								
											if (button && button != 'undefined') {
												button.disabled = true;
											}
										}
									}.bind(productAddToCartForm);
								
									productAddToCartForm.submitLight = function(button, url){
										if(this.validator) {
											var nv = Validation.methods;
											delete Validation.methods['required-entry'];
											delete Validation.methods['validate-one-required'];
											delete Validation.methods['validate-one-required-by-name'];
											// Remove custom datetime validators
											for (var methodName in Validation.methods) {
												if (methodName.match(/^validate-datetime-.*/i)) {
													delete Validation.methods[methodName];
												}
											}
								
											if (this.validator.validate()) {
												if (url) {
													this.form.action = url;
												}
												this.form.submit();
											}
											Object.extend(Validation.methods, nv);
										}
									}.bind(productAddToCartForm);
								//]]>
							 </script>
						  </div>
					   </div>
					   <script type="text/javascript">
						  var lifetime = 3600;
						  var expireAt = Mage.Cookies.expires;
						  if (lifetime > 0) {
							  expireAt = new Date();
							  expireAt.setTime(expireAt.getTime() + lifetime * 1000);
						  }
						  Mage.Cookies.set('external_no_cache', 1, expireAt);
					   </script>
					   
					   <div class="product-view"><div class="product-essential"></div></div>
					</div>      
					
                    <div class="grid_6 em-col-right em-sidebar">
					   <div class="no_quickshop block block-related">
						  <div class="block-title">
							 <strong><span>Related Products</span></strong>
						  </div>
						  <div class="block-content">
							 
							 <ol class="mini-products-list" id="block-related">
								<?php for($mp=0;$mp<3;$mp++){ 
										if($mp%2==0){$cls='odd';}else{$cls='even';}
								?>
								<li class="item <?php echo $cls;?>">
								   
								   <div class="product">
									  <a href="<?php echo $mItemSource[$mp]; ?>" title="Microsoft Wireless Optical Mouse 5000" class="product-image"><img src="<?php echo $mItemTumb[$mp]; ?>" width="70" height="70" alt="Microsoft Wireless Optical Mouse 5000"></a>
									  <div class="product-details">
										 <p class="product-name"><a href="<?php echo $mItemSource[$mp]; ?>"><?php echo str_replace($HTMLascii,$HTMLreal,$mItemTitle[$mp]); ?></a></p>
										 <div class="price-box">
											<span class="regular-price" id="product-price-162-related">
											<span class="price">$<?php echo $mItemRealPrice[$mp]; ?></span>                                    </span>
										 </div>
										 <a href="#"   onclick="affClick()"  class="link-wishlist" title="Add to Wishlist">Add to Wishlist</a>
									  </div>
								   </div>
								</li>
								<?php } ?>
							 </ol>
							 <script type="text/javascript">decorateList('block-related', 'none-recursive')</script>
						  </div>
						  <script type="text/javascript">
							 //<![CDATA[
							 $$('.related-checkbox').each(function(elem){
								 Event.observe(elem, 'click', addRelatedToProduct)
							 });
							 
							 var relatedProductsCheckFlag = false;
							 function selectAllRelated(txt){
								 if (relatedProductsCheckFlag == false) {
									 $$('.related-checkbox').each(function(elem){
										 elem.checked = true;
									 });
									 relatedProductsCheckFlag = true;
									 txt.innerHTML="unselect all";
								 } else {
									 $$('.related-checkbox').each(function(elem){
										 elem.checked = false;
									 });
									 relatedProductsCheckFlag = false;
									 txt.innerHTML="select all";
								 }
								 addRelatedToProduct();
							 }
							 
							 function addRelatedToProduct(){
								 var checkboxes = $$('.related-checkbox');
								 var values = [];
								 for(var i=0;i<checkboxes.length;i++){
									 if(checkboxes[i].checked) values.push(checkboxes[i].value);
								 }
								 if($('related-products-field')){
									 $('related-products-field').value = values.join(',');
								 }
							 }
							 //]]>
						  </script>
					   </div>
					</div>
                  <div class="clear"></div>
               </div>
            </div> 