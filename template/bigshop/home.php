<div class="wrapper_content">
               <div class="container_24 ">
                  <div class="grid_24 em-area01">
                     <div class="widget widget-static-block ">
                        <div class="special-gift" style="background-color: #f06287;">
                           <p><span class="gift"><img src="<?php echo $dirTemplate; ?>/skin/galabigshop/images/icon_gift.png" alt="sample-banner"/></span>Today we have a special gift fot potential customers. <a title="Login to your account" href="<?php echo $dirTemplate; ?>/login.html">Login to your account</a> and see how to get one <span class="close"><img src="<?php echo $dirTemplate; ?>/skin/galabigshop/images/icons_close.png" alt="sample-banner"/></span></p>
                        </div>
                        <div class="home-top-information">
                           <div class="grid_8 alpha">
                              <p><img src="<?php echo $dirTemplate; ?>/skin/galabigshop/images/icon_info_search.png" alt="sample-banner"/></p>
                              <p class="info-title">Search for everything</p>
                              <p class="phone"></p>
                           </div>
                           <div class="grid_8">
                              <p><img src="<?php echo $dirTemplate; ?>/skin/galabigshop/images/icon_info_shipping.png" alt="sample-banner"/></p>
                              <p class="info-title">Free shipping &amp; return</p>
                              <p class="phone"></p>
                           </div>
                           <div class="grid_8 omega">
                              <p><img src="<?php echo $dirTemplate; ?>/skin/galabigshop/images/icon_info_support.png" alt="sample-banner"/></p>
                              <p class="info-title">24/7 Help &amp; Support</p>
                              <p class="phone"></p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clear"></div>
               </div>
            </div>
            <div class="wrapper_content">
               <div class="wrapper_area0203">
                  <div class="container_24">
                     <div class="inner_slideshow">
                        <div class="grid_24">
                           
                           <style type="text/css">
							  ul.tag{
								padding:7px 20px 8px;
							  }
							  ul.tag li{
								background: #f06287 none repeat scroll 0 0;
								color: White;
								margin: 2px;
								padding: 5px;
								width: auto;
								float: left;
							  }
							  ul.tag li a{
								color: white;
							  }
							  ul.tag li:hover{
								color: white;
								background: #46b08d none repeat scroll 0 0;
							  }
						   
                              #rev_slider_1_1490, #rev_slider_1_1490_wrapper { width:100%; height:340px;}
                              @media only screen and (min-width: 1200px)  {
                              #rev_slider_1_1490, #rev_slider_1_1490_wrapper { width:100%; height:340px;}
                              }
                              @media only screen and (min-width: 1000px) and (max-width: 1199px) {
                              #rev_slider_1_1490, #rev_slider_1_1490_wrapper { width:700px; height:270px;}
                              }
                              @media only screen and (min-width: 600px) and (max-width: 999px) {
                              #rev_slider_1_1490, #rev_slider_1_1490_wrapper { width:520px; height:200px;}
                              }
                              @media only screen and (min-width: 450px) and (max-width: 599px) {
                              #rev_slider_1_1490, #rev_slider_1_1490_wrapper { width:430px; height:166px;}
                              }
                              @media only screen and (min-width: 380px) and (max-width: 449px) {
                              #rev_slider_1_1490, #rev_slider_1_1490_wrapper { width:300px; height:115px;}
                              }
                              @media only screen and (min-width: 0px) and (max-width: 379px) {
                              #rev_slider_1_1490, #rev_slider_1_1490_wrapper { width:280px; height:108px;}
                              }
							  img.sliderimgasb{
								width:auto !important;
								height:60% !important;
							  }
							  p.pasb{
								word-wrap: break-word;
							  }
							  p.ssb{
								margin-top: 8% !important;
								word-wrap: break-word;
							  }
                           </style>
                           <div id="rev_slider_1_1490_wrapper" class="rev_slider_wrapper" style="margin:0px auto;background-color:#ffffff;padding:10px;margin-top:0px;margin-bottom:0px;">
							   <div id="rev_slider_1_1490" class="rev_slider" style="display:none;">
								  <ul>
									 <!-- THE BOXSLIDE EFFECT EXAMPLES  WITH LINK ON THE MAIN SLIDE EXAMPLE -->
									 <?php for($sh=8;$sh<11;$sh++){ ?>
									 <li data-transition="demo" data-slotamount="7" data-link="<?php echo $reSingle[$sh]; ?>" data-delay="5000">
										<img src="<?php echo $rePict[$sh]; ?>" class="sliderimgasb" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$sh]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$sh]); ?>"/>
										<div class="caption lft bkg_color" data-x="0" data-y="250" data-speed="1000" data-start="500" data-easing="easeInOutExpo">
										   <p>bkg_color</p>
										</div>
										<div class="caption lfl " data-x="20" data-y="258" data-speed="1000" data-start="1500" data-easing="easeInOutQuint">
										   <p class="ssb" style="font-size:270%;color:#fff;font-family:Roboto;font-weight:200;line-height:1">
										   <?php
										    preg_match('/^(?>\S+\s*){1,5}/', $reTitle[$sh], $match);
											echo rtrim($match[0]);
										   ?>
										   </p>
										</div>
										<div class="caption lfr " data-x="20" data-y="309" data-speed="1000" data-start="2000" data-easing="easeInOutCubic">
										   <p class="pasb" style="font-family:Roboto;color:#fff;font-size:100%;font-weight:300;font-style:italic;line-height:1"><?php //echo $reTitle[$sh]; ?></p>
										</div>
										<div class="caption lfb btn" data-x="585" data-y="285" data-speed="1000" data-start="3000" data-easing="easeInOutBounce"><a href="#" style="line-height:1;font-weight:400;font-family:Roboto;">START AT $<?php echo $homeRealPrice[$sh]; ?></a></div>
									 </li>
									 <?php } ?>
								  </ul>
								  <div class="tp-bannertimer"></div>
							   </div>
							</div>
                           <script type="text/javascript">
                              var tpj=jQuery;
                              				tpj.noConflict();
                              			
                              var revapi1;
                              
                              tpj(window).load(function() {
                              
                              if (tpj.fn.cssOriginal != undefined)
                              	tpj.fn.css = tpj.fn.cssOriginal;
                              
                              if(tpj('#rev_slider_1_1490').revolution == undefined)
                              	revslider_showDoubleJqueryError('#rev_slider_1_1490');
                              else
                                 revapi1 = tpj('#rev_slider_1_1490').show().revolution(
                              	{
                              		delay:5000,
                              		startwidth:880,
                              		startheight:340,
                              		hideThumbs:200,
                              		
                              		thumbWidth:100,
                              		thumbHeight:50,
                              		thumbAmount:5,
                              		
                              		navigationType:"bullet",
                              		navigationArrows:"none",
                              		navigationStyle:"round",
                              		
                              		touchenabled:"on",
                              		onHoverStop:"on",
                              		
                              		navigationHAlign:"center",
                              		navigationVAlign:"bottom",
                              		navigationHOffset:0,
                              		navigationVOffset:20,
                              		
                              		soloArrowLeftHalign:"left",
                              		soloArrowLeftValign:"center",
                              		soloArrowLeftHOffset:"20",
                              		soloArrowLeftVOffset:"0",
                              
                              		soloArrowRightHalign:"right",
                              		soloArrowRightValign:"center",
                              		soloArrowRightHOffset:"20",
                              		soloArrowRightVOffset:"0",
                              		
                              		shadow:0,
                              		fullWidth:"off",
                              
                              		stopLoop:"off",
                              		stopAfterLoops:-1,
                              		stopAtSlide:-1,
                              
                              		shuffle:"off",
                              		
                              		hideSliderAtLimit:0,
                              		hideCaptionAtLimit:0,
                              		hideAllCaptionAtLilmit:0
                              	});
                              });
                           </script>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="wrapper_content">
               <div class="container_24 ">
			   <?php include('sidebar.php');?>
                  <div class="grid_18 em-main-wrapper">
					 <div class="tabs-widget ">
					   <div class="tabs_wrapper" id="emtabs_9eb7b1ac299035218a7c9843074e5146">
						  <ul class="tabs_control">
							 <li>
								<a href="#tab_emtabs_9eb7b1ac299035218a7c9843074e5146_1"> <span class="icon tab_emtabs_9eb7b1ac299035218a7c9843074e5146_1"></span>All</a>
							 </li>
							 
							 <li>
								<a href="#tab_emtabs_9eb7b1ac299035218a7c9843074e5146_2"> <span class="icon tab_emtabs_9eb7b1ac299035218a7c9843074e5146_2"></span>Best Seller</a>
							 </li>
						  </ul>
						  <div class="tab_content">
							 <div id="tab_emtabs_9eb7b1ac299035218a7c9843074e5146_1" class="tab-item content_tab_emtabs_9eb7b1ac299035218a7c9843074e5146_1">
								<div class="category-products">
								   <div class="widget em-widget-products-grid custom" id="ajaxproducts-99">
									  <div class="widget-products">
										 <ul class="ajax-product products-grid last even">
											<?php foreach($homeSingle as $h=>$datas){?>
											<li class="box  item <?php if($h==0){echo'first';}?>" style=" width:280px; ">
											   <!--show label product - label extension is required-->
											   <!--<span class="productlabels_icons">
											   <span class="label new">
											   <span>
											   New        </span>
											   </span>
											   <span class="label bestseller">
											   <span>
											   Best        </span>
											   </span>
											   </span>	-->											
											   <a href="<?php echo $homeSingle[$h]; ?>" title="<?php echo substr($homeTitle[$h],0,25); ?>" class="product-image">							
											   <img href="<?php echo $homeSingle[$h]; ?>" src="<?php echo $homeTumb[$h]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$homeTitle[$h]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$homeTitle[$h]); ?>" style="width: 100%;"/></a>
											   <div class="product-shop">
												  <div class="f-fix">
													 <!--product name-->
													 <h3 class="product-name"><a href="<?php echo $homeSingle[$h]; ?>" title="<?php echo substr($homeTitle[$h],0,25); ?>"> <?php echo substr($homeTitle[$h],0,25); ?> </a></h3>
													 <!--product description-->
													 <div class="price_review">
														<!--product price-->
														<div class="price-box">
														   <span class="regular-price" id="product-price-<?php echo $h;?>-widget-new-list">
														   <span class="price">$<?php echo $homeRealPrice[$h]; ?></span>                                    </span>
														</div>
														<!--product reviews-->
														<div class="ratings">
														   <div class="rating-box">
															  <div class="rating" style="width:67%"></div>
														   </div>
														   <span class="amount"><a href="#" onclick="setLocation('<?php echo $homeSingle[$h]; ?>')"><?php echo rand(0,6);?> Reviews</a></span>
														</div>
													 </div>
													 <div class="actions js-addcart hover-slide" name="product_<?php echo $h;?>" style="display: none;">
														<!--product add to cart-->
														<div class="actions-cart"><button type="button" onclick="setLocation('<?php echo $homeSingle[$h]; ?>')" title="Add to Cart" class="button btn-cart" ><span><span>Add to Cart</span></span></button></div>
														<!--product add to compare-wishlist-->
														<ul class="add-to-links">
														   <li><a href="<?php echo $homeSingle[$h]; ?>" class="link-wishlist" title="Add to Wishlist"><span class="text">Add to Wishlist</span></a></li>
														   <li><span class="separator">|</span><a href="<?php echo $homeSingle[$h]; ?>" class="link-compare" title="Add to Compare"><span class="text">Add to Compare</span></a></li>
														</ul>
													 </div>
												  </div>
											   </div>
											</li>
											<?php } ?>
											
										 </ul>
									  </div>
								   </div>
								   <!--<p class="load-more-image">
									  <a class="load-more" id="ajaxproducts-99-next" href="#">Load more</a>
								   </p>-->
								   <script type="text/javascript">
									  jQuery('#ajaxproducts-99 .widget-products ul.products-grid').infinitescroll({
										navSelector  	: "#ajaxproducts-99-next:last",
										nextSelector 	: "#ajaxproducts-99-next:last",
										itemSelector 	: "#ajaxproducts-99 .widget-products ul.products-grid li.item",
										dataType	 	: 'html',
										loading: {
											img				: "<?php echo $dirTemplate; ?>/skin/galabigshop/images/ajax-loader.gif",
										},
										maxPage         : 4,
														state			: {
											isPaused : true
										},
														path: function(index) {
											return '<?php echo $dirTemplate; ?>/ajaxproduct.html';
										}
									  }, function(newElements, data, url){
															jQuery("#ajaxproducts-99-next:last").show();
														setTimeout(function(){
											afterLoadAjax('#ajaxproducts-99');                    
												   },500);
										 });
												jQuery('#ajaxproducts-99-next').click(function(){
										jQuery('#ajaxproducts-99 .widget-products ul.products-grid').infinitescroll('retrieve');
										return false;
									  });
											
								   </script>
								</div>
								<script type="text/javascript">
								   decorateGeneric($$('ul.products-grid'),['last','first','odd','even']);
								</script>	
							 </div>
							 <div id="tab_emtabs_9eb7b1ac299035218a7c9843074e5146_2" class="tab-item content_tab_emtabs_9eb7b1ac299035218a7c9843074e5146_2">
								<div class="category-products">
								   <div class="widget em-widget-products-grid custom" id="ajaxproducts-14">
									  <div class="widget-products">
										 <ul class="ajax-product products-grid last odd">
											<?php foreach($reTumb as $tr=>$dtg){ ?>
											<li class="box  item <?php if($h==0){echo'first';}?>" style=" width:280px; ">
											   <!--show label product - label extension is required-->											
											   <a href="<?php echo $reSingle[$tr]; ?>" title="<?php echo substr($reTitle[$tr],0,17); ?>" class="product-image">							
											   <img href="<?php echo $homeSingle[$h]; ?>"  src="<?php echo $reTumb[$tr]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$tr]); ?>" style="width: 100%;"/></a>
											   <div class="product-shop">
												  <div class="f-fix">
													 <!--product name-->
													 <h3 class="product-name"><a href="<?php echo $reSingle[$tr]; ?>" title=" <?php echo substr($reTitle[$tr],0,17); ?>"> <?php echo substr($reTitle[$tr],0,17); ?></a></h3>
													 <!--product description-->
													 <div class="price_review">
														<!--product price-->
														<div class="price-box">
														   <span class="regular-price" id="product-price-45-widget-new-list">
														   <span class="price">$<?php echo $reRealPrice[$tr]; ?></span>                                    </span>
														</div>
														<!--product reviews-->
														<div class="ratings">
														   <div class="rating-box">
															  <div class="rating" style="width:67%"></div>
														   </div>
														   <span class="amount"><a href="#" onclick="setLocation('<?php echo $reSingle[$tr]; ?>')"><?php echo rand(0,6);?> Reviews</a></span>
														</div>
													 </div>
													 <div class="actions js-addcart hover-slide" name="product_<?php echo $dtt;?>" style="display: none;">
														<!--product add to cart-->
														<div class="actions-cart"><button type="button" onclick="setLocation('<?php echo $reSingle[$tr]; ?>')" title="Add to Cart" class="button btn-cart" ><span><span>Add to Cart</span></span></button></div>
														
													 </div>
												  </div>
											   </div>
											</li>
											<?php } ?>
										 </ul>
									  </div>
								   </div>
								   <p class="load-more-image">
									  <a class="load-more" id="ajaxproducts-14-next" href="#">Load more</a>
								   </p>
								   <script type="text/javascript">
									  jQuery('#ajaxproducts-14 .widget-products ul.products-grid').infinitescroll({
										navSelector  	: "#ajaxproducts-14-next:last",
										nextSelector 	: "#ajaxproducts-14-next:last",
										itemSelector 	: "#ajaxproducts-14 .widget-products ul.products-grid li.item",
										dataType	 	: 'html',
										loading: {
											img				: "./skin/galabigshop/images/ajax-loader.gif",
										},
										maxPage         : 3,
														state			: {
											isPaused : true
										},
														path: function(index) {
											return './ajaxproduct.html';
										}
									  }, function(newElements, data, url){
															jQuery("#ajaxproducts-14-next:last").show();
														setTimeout(function(){
											afterLoadAjax('#ajaxproducts-14');                    
												   },500);
										 });
												jQuery('#ajaxproducts-14-next').click(function(){
										jQuery('#ajaxproducts-14 .widget-products ul.products-grid').infinitescroll('retrieve');
										return false;
									  });
											
								   </script>
								</div>
								<script type="text/javascript">
								   decorateGeneric($$('ul.products-grid'),['last','first','odd','even']);
								</script>	
							 </div>
							 
							
							
						  </div>
					   </div>
					</div>
                  </div>
                  <div class="clear"></div>
               </div>
            </div>