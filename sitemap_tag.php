<?php 
header("Content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" ?>';
include('./func/data.php');

//Halaman
$datahal = 500;
if(isset($_GET['p'])){
    $noPage = $_GET['p'];} 
else $noPage = 1;
$offset = ($noPage - 1) * $datahal;

?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php
	$sitemap = json_decode(file_get_contents($linkJSON.'/?'.$cekKey.'list=keyword&page='.$noPage.'&viewpage='.$datahal.'&random=y&aff='.$categoryAff), true);
		for($s=0;$s<count($sitemap['results']);$s++){
	?>
	<url>
        <loc><?php echo $tagPerm . $sitemap['results'][$s]['source'] . $suffPerm; ?></loc>
        <changefreq>daily</changefreq>
        <priority>0.90</priority>
    </url>
	<?php } ?>
</urlset>