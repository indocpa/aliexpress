<?php

/** identify template **/
$fileConfig = 'config.php';
if(file_exists($fileConfig)) include('func/data.php');

if(isset($_GET['lp'])){
	include('lp/index.php');
}else{
	if(!file_exists($fileConfig)){
		Header('Location: agcadmin/install/');
	}else{
		include($webTemplate);
	}
}

?>